import { CElement, ELEMENT_NAMES } from "../../data/table.ts";
import { Logger } from "../../logger.ts";
import { Atom } from "../../representation/atom.ts";
import { Molecule } from "../../representation/molecule.ts";
import { link_chain, saturate_hydrogen } from "../helpers.ts";
import { chain_names, IUPACChain, IUPACModName } from "./mod.ts";
const L = new Logger("naming/iupac/build")


export function build_iupac_chain(c: IUPACChain) {
    let len = 1
    let name = c.name
    let cyclic = false
    const chain: Atom[] = []

    const is_carbon_chain = Object.keys(chain_names).find(n => c.name.endsWith(n))
    if (is_carbon_chain) {
        if (name.startsWith("cyclo")) {
            cyclic = true, name = name.substring("cyclo".length)
        }
        for (const tname in chain_names) {
            if (name == tname) {
                len = chain_names[name]
            }
        }

        for (let i = 0; i < len; i++) {
            chain.push(new Atom("C"))
        }
        link_chain(chain)
        if (cyclic) chain[0].bind(chain[chain.length - 1])
    } else {
        const element = Object.entries(ELEMENT_NAMES).find(([v, _]) => v == c.name)?.[1] as CElement | undefined
        if (!element) throw new Error("blub");
        chain.push(new Atom(element))
    }



    // place explicitly positioned attachments
    for (const af of c.mods) {
        for (const p of af.positions ?? []) {
            if (af.name) attach_suffix(af.name, chain, p)
            if (af.attachment) {
                const subchain = build_iupac_chain(af.attachment)
                chain[p].bind(subchain[0])
            }
            af.count -= 1
        }
    }

    // place left-over attachments without explicite position
    for (const af of c.mods) {
        while (af.count-- > 0) {
            if (af.attachment) {
                const index = chain.findIndex((a) => {
                    const free_bonds = a.free_bond_count
                    return free_bonds >= 1
                })
                if (index != -1) {
                    const subchain = build_iupac_chain(af.attachment)
                    chain[index].bind(subchain[0])
                }
                else L.warn(`could not attach implicitly positioned -${af.attachment.name}.`)
            }
            if (af.name) {
                const index = chain.findIndex((a, i) => {
                    const free_bonds = a.free_bond_count
                    const free_bonds_next = chain[i + 1]?.free_bond_count

                    if (af.name == "en") return free_bonds >= 1 && free_bonds_next >= 1
                    if (af.name == "in") return free_bonds >= 2 && free_bonds_next >= 2
                    if (af.name == "ol") return free_bonds >= 1
                    if (af.name == "al") return free_bonds >= 2
                    if (af.name == "on") return free_bonds >= 2 && i != 0 && i != chain.length - 1
                    if (af.name == "oxo") return free_bonds >= 2
                    if (af.name == "hydroxy") return free_bonds >= 1
                    if (af.name == "säure") return free_bonds >= 3
                    return true
                })
                if (index != -1) attach_suffix(af.name, chain, index)
                else L.warn(`could not attach implicitly positioned -${af.name}.`)
            }
        }
    }

    const m = new Molecule(chain[0])
    if (is_carbon_chain) saturate_hydrogen(m)

    // TODO generalize
    for (const sf of c.mods) {
        if (sf.name == "yl") {
            [...chain[0].bonds].find(b => b.other(chain[0]).element == "H")?.destroy()
        }
    }

    return chain
}

function attach_suffix(name: IUPACModName, chain: Atom[], index: number) {
    if (name == "on" || name == "al" || name == "oxo") {
        chain[index].bind(new Atom("O"), "double")
    }
    if (name == "ol" || name == "hydroxy") {
        chain[index].bind(new Atom("O"), "single")
    }
    if (name == "säure") {
        chain[index].bind(new Atom("O").bind(new Atom("H")))
        chain[index].bind(new Atom("O"), "double")
    }
    if (name == "en") {
        chain[index].get_bond_to(chain[index + 1])!.type = "double"
    }
    if (name == "in") {
        chain[index].get_bond_to(chain[index + 1])!.type = "triple"
    }
}