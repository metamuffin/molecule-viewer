export const chain_names: Record<string, number> = {
    "meth": 1,
    "eth": 2,
    "prop": 3,
    "but": 4,
    "pent": 5,
    "hex": 6,
    "hept": 7,
    "oct": 7,
    "non": 9,
    "dec": 10,
    "undec": 11,
    "dodec": 12,
}
export const count_prefixes: Record<string, number> = {
    "mono": 1,
    "di": 2,
    "tri": 3,
    "tetra": 4,
    "penta": 5,
    "hexa": 6,
    "hepta": 7,
    "octa": 8,
    "nona": 9,
    "deca": 10,
    "undeca": 11,
    "dodeca": 12,
}

export type IUPACModName = "an" | "en" | "in" | "on" | "ol" | "al" | "yl" | "oxo" | "hydroxy" | "säure" | "aza"

export interface IUPACChain {
    mods: IUPACMod[]
    name: string
}

export interface IUPACMod {
    attachment?: IUPACChain
    name?: IUPACModName
    count: number
    positions?: number[]
}

