import { CElement } from "../../data/table.ts";
import { SMILESChain, SMILESEl } from "./mod.ts";

// // two-letter symbols must precede one-letter symbols with the same first letter
// const elements = ["Cl", "Br", "B", "C", "N", "O", "P", "S", "F", "I"]

export function parse_smiles(s: string): SMILESChain {
    const chain: SMILESChain = []
    const default_end = (): SMILESEl => ({ attachments: [], bond: "single", cross_bonds: [], element: "" as CElement })
    let end: SMILESEl = default_end()

    let _bracket_type: string | undefined;
    let bracket_level = 0
    let bracket_acc = ""

    const maybe_shift = () => {
        if (end.element != "") {
            chain.push(end)
            end = default_end()
        }
    }

    for (const char of s) {
        if (bracket_level > 0) {
            if (char == ")" || char == "]") bracket_level--
            if (bracket_level == 0) {
                const attachment = parse_smiles(bracket_acc)
                end.attachments.push(attachment)
            } else bracket_acc += char
            continue
        }

        if ("bcnops".split("").includes(char)) {
            maybe_shift()
            end.implicit_aromatic_ring_bond = true
            end.element += char.toUpperCase()
        } else if (char.toLowerCase() != char) {
            maybe_shift()
            // uppercase letter 
            end.element += char
        } else if (char.toUpperCase() != char) {
            // lowercase letter
            end.element += char
        }

        if (char == "-") maybe_shift(), end.bond = "single"
        if (char == "=") maybe_shift(), end.bond = "double"
        if (char == "#") maybe_shift(), end.bond = "triple"

        if (!Number.isNaN(parseInt(char))) {
            end.cross_bonds.push(parseInt(char))
        }
        if (char == "(" || char == "[") {
            _bracket_type = char
            bracket_level++
            bracket_acc = ""
        }
    }
    maybe_shift()

    return chain
}

