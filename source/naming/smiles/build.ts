import { CElement } from "../../data/table.ts";
import { Atom } from "../../representation/atom.ts";
import { Molecule } from "../../representation/molecule.ts";
import { saturate_hydrogen } from "../helpers.ts";
import { SMILESChain } from "./mod.ts";

export function build_smiles(s: SMILESChain): Molecule {

    const { chain, cross_bond_lookup, implicit_aromatic } = build_smiles_chain(s)

    cross_bond_lookup.forEach(v => {
        if (v.length != 2) return console.warn("cross bond between more or less than 2 atoms")
        v[0].bind(v[1])
    })


    for (const a of implicit_aromatic) {
        if (a.free_bond_count < 1) continue
        const b = a.bond_arr.find(b => {
            const a2 = b.other(a)
            if (!implicit_aromatic.has(a2)) return
            if (a2.free_bond_count < 1) return
            return true
        })
        if (!b) continue
        b.type = "double"
        implicit_aromatic.delete(b.a)
        implicit_aromatic.delete(b.b)
    }


    const m = new Molecule(chain[0])
    saturate_hydrogen(m)

    return m
}

export function build_smiles_chain(s: SMILESChain, alpha?: Atom) {
    const chain: Atom[] = []
    if (alpha) chain.push(alpha)

    const implicit_aromatic = new Set<Atom>()
    const cross_bond_lookup = new Map<number, Atom[]>()
    const register_cross_bond = (k: number, v: Atom) => {
        if (!cross_bond_lookup.has(k)) cross_bond_lookup.set(k, [])
        cross_bond_lookup.get(k)?.push(v)
    }

    for (const i of s) {
        const a = new Atom(i.element as CElement)
        i.cross_bonds.forEach(n => {
            register_cross_bond(n, a)
        })
        for (const at of i.attachments) {
            const sbr = build_smiles_chain(at, a)
            sbr.cross_bond_lookup.forEach((v, k) => {
                v.forEach(a => register_cross_bond(k, a))
            })
            sbr.implicit_aromatic.forEach(a => implicit_aromatic.add(a))
        }
        const last_atom = chain[chain.length - 1]
        if (last_atom) {
            last_atom.bind(a, i.bond)
        }
        if (i.implicit_aromatic_ring_bond) implicit_aromatic.add(a)
        chain.push(a)
    }

    return { chain, cross_bond_lookup, implicit_aromatic }
}