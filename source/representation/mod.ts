
export interface SMolecule {
    atoms: SAtom[]
    bonds: SBond[]
}

export interface SBond {
    type: "single" | "double" | "triple"
    a: number,
    b: number
}
export interface SAtom {
    element: "C" | "H" | "O"
}
