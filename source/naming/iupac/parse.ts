import { ELEMENT_NAMES } from "../../data/table.ts";
import { Logger } from "../../logger.ts";
import { chain_names, count_prefixes, IUPACChain, IUPACMod, IUPACModName } from "./mod.ts";
const L = new Logger("naming/iupac/parse")

export interface IUPACParseOptions { no_prefix?: boolean, no_suffix?: boolean }
export function parse_iupac_chain(s: string): IUPACChain {
    const { prefixes, rest } = parse_prefixes(s)
    if (rest.length) L.warn(`prefix parsing left over rest: ${JSON.stringify(rest)}`);

    L.log(prefixes);

    const chain = prefixes.pop()
    L.log(chain);

    if (!chain) throw new Error("primary chain missing");
    if (!chain.attachment) throw new Error("primary chain is invalid");
    if (chain.positions) L.warn("primary chain should not have positions");
    if (chain.count != 1) L.warn("count of primary chain should be one");
    return {
        name: chain.attachment.name,
        mods: [...prefixes, ...chain.attachment!.mods]
    }

}

function parse_suffixes(s: string): { rest: string, suffixes: IUPACMod[] } {
    L.log("parse_suffixes", JSON.stringify(s));

    // TODO this is a rather crude fix
    // remove a's at the start cause by ie 'but*a*-1,3-dien'
    if (s.startsWith("a") && !s.startsWith("amin") && !s.startsWith("an")) s = s.substring(1)

    const suffixes: IUPACMod[] = []
    let parts = s.split("-")

    let count = 1
    let positions: number[] = []

    while (parts.length) {
        let p = parts.shift()!
        const p_orig = p

        const np = p.split(",").map(n => parseInt(n))
        if (!np.map(Number.isNaN).reduce((a, b) => a || b, false)) {
            positions = np.map(n => n - 1) // convert to 0-based indexing
            L.log({ np, p, positions });
            p = ""
        }

        for (const prefix in count_prefixes) {
            if (p.startsWith(prefix)) {
                count = count_prefixes[prefix]
                p = p.substring(prefix.length)
            }
        }

        const suffix_names = ["an", "en", "in", "ol", "al", "on", "yl", "säure"]
        for (const sf of suffix_names) {
            if (p.startsWith(sf)) {
                suffixes.push({ name: sf as IUPACModName, count, positions })
                p = p.substring(sf.length)
                count = 1
                positions = []
            }
        }

        if (p.length) parts = [p, ...parts]
        if (p.length >= p_orig.length) { L.log("could not parse all suffixes, stuck at ", JSON.stringify(p)); break }
    }
    let rest = parts.join("-")
    if (positions.length) rest = positions.map(n => n + 1).join(",") + "-" + rest
    if (count != 1) {
        const c = Object.entries(count_prefixes).find(([_, b]) => b == count)
        if (c) rest = c[0] + "-" + rest
    }
    L.log("parse_suffixes end", { rest, suffixes, count })
    return { rest, suffixes }
}

function parse_prefixes(s: string): { rest: string, prefixes: IUPACMod[] } {
    L.log("parse_prefixes", JSON.stringify(s));

    const prefixes: IUPACMod[] = []
    let parts = s.split("-")

    let count = 1
    let positions: number[] = []

    while (parts.length) {
        let p = parts.shift()!
        const p_orig = p

        const np = p.split(",").map(n => parseInt(n))
        if (!np.map(Number.isNaN).reduce((a, b) => a || b, false)) {
            positions = np.map(n => n - 1) // convert to 0-based indexing
            p = ""
        }

        for (const prefix in count_prefixes) {
            if (p.startsWith(prefix)) {
                count = count_prefixes[prefix]
                p = p.substring(prefix.length)
            }
        }

        const possible_prefixed_suffixes = ["hydroxy", "oxo"]
        const possible_prefixes = [
            ...Object.keys(chain_names),
            ...Object.keys(chain_names).map(e => "cyclo" + e),
            ...Object.keys(ELEMENT_NAMES),
            ...possible_prefixed_suffixes
        ]

        for (const prefix of possible_prefixes) {
            if (p.startsWith(prefix)) {
                if (possible_prefixed_suffixes.includes(prefix)) {
                    if (p.length) parts = [p.substring(prefix.length), ...parts]
                    p = ""
                    prefixes.push({
                        count,
                        positions: positions.length ? positions : undefined,
                        name: prefix as IUPACModName
                    })
                    positions = [] // reset for the next one to be pushed
                    count = 1
                    break
                }
                const { suffixes, rest } = parse_suffixes(p.substring(prefix.length) + parts.join("-"))
                p = ""
                parts = rest.split("-")
                const attachment: IUPACChain = {
                    mods: suffixes,
                    name: prefix,
                }
                prefixes.push({
                    count,
                    positions: positions.length ? positions : undefined,
                    attachment
                })
                positions = [] // reset for the next one to be pushed
                count = 1
            }
        }
        if (p.length) return { rest: [p_orig, ...parts].join("-"), prefixes }
    }

    L.log("naming/iupac", { rest: parts.join("-"), prefixes })
    return { rest: parts.join("-"), prefixes }
}


