export type CElement = "H" | "He" | "Li" | "Be" | "B" | "C" | "N" | "O" | "F" | "Ne" | "Na" | "Mg" | "Al" | "Si" | "P" | "S" | "Cl" | "Ar" | "K" | "Ca" | "Sc" | "Ti" | "V" | "Cr" | "Mn" | "Fe" | "Co" | "Ni" | "Cu" | "Zn" | "Ga" | "Ge" | "As" | "Se" | "Br" | "Kr" | "Rb" | "Sr" | "Y" | "Zr" | "Nb" | "Mo" | "Tc" | "Ru" | "Rh" | "Pd" | "Ag" | "Cd" | "In" | "Sn" | "Sb" | "Te" | "I" | "Xe" | "Cs" | "Ba" | "La" | "Ce" | "Pr" | "Nd" | "Pm" | "Sm" | "Eu" | "Gd" | "Tb" | "Dy" | "Ho" | "Er" | "Tm" | "Yb" | "Lu" | "Hf" | "Ta" | "W" | "Re" | "Os" | "Ir" | "Pt" | "Au" | "Hg" | "Tl" | "Pb" | "Bi" | "Po" | "At" | "Rn" | "Fr" | "Ra" | "Ac" | "Th" | "Pa" | "U" | "Np" | "Pu" | "Am" | "Cm" | "Bk" | "Cf" | "Es" | "Fm" | "Md" | "No" | "Lr" | "Rf" | "Db" | "Sg" | "Bh" | "Hs" | "Mt" | "Ds" | "Rg" | "Cn" | "Nh" | "Fl" | "Mc" | "Lv" | "Ts" | "Og" | "Uue"

export const PROTON_COUNT: Record<CElement, number> = { H: 1, He: 2, Li: 3, Be: 4, B: 5, C: 6, N: 7, O: 8, F: 9, Ne: 10, Na: 11, Mg: 12, Al: 13, Si: 14, P: 15, S: 16, Cl: 17, Ar: 18, K: 19, Ca: 20, Sc: 21, Ti: 22, V: 23, Cr: 24, Mn: 25, Fe: 26, Co: 27, Ni: 28, Cu: 29, Zn: 30, Ga: 31, Ge: 32, As: 33, Se: 34, Br: 35, Kr: 36, Rb: 37, Sr: 38, Y: 39, Zr: 40, Nb: 41, Mo: 42, Tc: 43, Ru: 44, Rh: 45, Pd: 46, Ag: 47, Cd: 48, In: 49, Sn: 50, Sb: 51, Te: 52, I: 53, Xe: 54, Cs: 55, Ba: 56, La: 57, Ce: 58, Pr: 59, Nd: 60, Pm: 61, Sm: 62, Eu: 63, Gd: 64, Tb: 65, Dy: 66, Ho: 67, Er: 68, Tm: 69, Yb: 70, Lu: 71, Hf: 72, Ta: 73, W: 74, Re: 75, Os: 76, Ir: 77, Pt: 78, Au: 79, Hg: 80, Tl: 81, Pb: 82, Bi: 83, Po: 84, At: 85, Rn: 86, Fr: 87, Ra: 88, Ac: 89, Th: 90, Pa: 91, U: 92, Np: 93, Pu: 94, Am: 95, Cm: 96, Bk: 97, Cf: 98, Es: 99, Fm: 100, Md: 101, No: 102, Lr: 103, Rf: 104, Db: 105, Sg: 106, Bh: 107, Hs: 108, Mt: 109, Ds: 110, Rg: 111, Cn: 112, Nh: 113, Fl: 114, Mc: 115, Lv: 116, Ts: 117, Og: 118, Uue: 119 }
export const ACCEPTED_BONDS: Record<CElement, number> = { H: 1, He: 2, Li: 1, Be: 2, B: 3, C: 4, N: 3, O: 2, F: 1, Ne: 0, Na: 1, Mg: 2, Al: 3, Si: 4, P: 3, S: 2, Cl: 1, Ar: 0, K: 1, Ca: 2, Sc: 2, Ti: 2, V: 2, Cr: 1, Mn: 2, Fe: 2, Co: 2, Ni: 2, Cu: 1, Zn: 2, Ga: 3, Ge: 4, As: 3, Se: 2, Br: 1, Kr: 0, Rb: 1, Sr: 2, Y: 2, Zr: 2, Nb: 1, Mo: 1, Tc: 2, Ru: 1, Rh: 1, Pd: -10, Ag: 1, Cd: 2, In: 3, Sn: 4, Sb: 3, Te: 2, I: 1, Xe: 0, Cs: 1, Ba: 2, La: 2, Ce: 2, Pr: 2, Nd: 2, Pm: 2, Sm: 2, Eu: 2, Gd: 2, Tb: 2, Dy: 2, Ho: 2, Er: 2, Tm: 2, Yb: 2, Lu: 2, Hf: 2, Ta: 2, W: 2, Re: 2, Os: 2, Ir: 2, Pt: 1, Au: 1, Hg: 2, Tl: 3, Pb: 4, Bi: 3, Po: 2, At: 1, Rn: 0, Fr: 1, Ra: 2, Ac: 2, Th: 2, Pa: 2, U: 2, Np: 2, Pu: 2, Am: 2, Cm: 2, Bk: 2, Cf: 2, Es: 2, Fm: 2, Md: 2, No: 2, Lr: 3, Rf: 2, Db: 2, Sg: 2, Bh: 2, Hs: 2, Mt: 2, Ds: 2, Rg: 2, Cn: 2, Nh: 3, Fl: 4, Mc: 3, Lv: 2, Ts: 1, Og: 0, Uue: 1 }
export const SHELL_COUNT: Record<CElement, number> = { H: 1, He: 1, Li: 2, Be: 2, B: 2, C: 2, N: 2, O: 2, F: 2, Ne: 2, Na: 3, Mg: 3, Al: 3, Si: 3, P: 3, S: 3, Cl: 3, Ar: 3, K: 4, Ca: 4, Sc: 4, Ti: 4, V: 4, Cr: 4, Mn: 4, Fe: 4, Co: 4, Ni: 4, Cu: 4, Zn: 4, Ga: 4, Ge: 4, As: 4, Se: 4, Br: 4, Kr: 4, Rb: 5, Sr: 5, Y: 5, Zr: 5, Nb: 5, Mo: 5, Tc: 5, Ru: 5, Rh: 5, Pd: 4, Ag: 5, Cd: 5, In: 5, Sn: 5, Sb: 5, Te: 5, I: 5, Xe: 5, Cs: 6, Ba: 6, La: 6, Ce: 6, Pr: 6, Nd: 6, Pm: 6, Sm: 6, Eu: 6, Gd: 6, Tb: 6, Dy: 6, Ho: 6, Er: 6, Tm: 6, Yb: 6, Lu: 6, Hf: 6, Ta: 6, W: 6, Re: 6, Os: 6, Ir: 6, Pt: 6, Au: 6, Hg: 6, Tl: 6, Pb: 6, Bi: 6, Po: 6, At: 6, Rn: 6, Fr: 7, Ra: 7, Ac: 7, Th: 7, Pa: 7, U: 7, Np: 7, Pu: 7, Am: 7, Cm: 7, Bk: 7, Cf: 7, Es: 7, Fm: 7, Md: 7, No: 7, Lr: 7, Rf: 7, Db: 7, Sg: 7, Bh: 7, Hs: 7, Mt: 7, Ds: 7, Rg: 7, Cn: 7, Nh: 7, Fl: 7, Mc: 7, Lv: 7, Ts: 7, Og: 7, Uue: 8 }

export const DISPLAY_COLOR: { [key in CElement]?: string } = {
    C: "#555555",
    H: "#ffffff",
    O: "#ff0000",
    N: "#0000ff",
    Br: "#fc8600",
    S: "#fcde00",
    I: "#be3dff",
    Cl: "#06aa03",
    F: "#51ef4f",
    P: "#f74f2a"
}

export const ELEMENT_NAMES: { [key in string]?: CElement } = {
    "stickstoff": "N",
    "amino": "N",
    "amin": "N",
    "äther": "O",
    "ether": "O",
    "sauerstoff": "O",
    "fluor": "F",
    "chlor": "Cl",
    "brom": "Br",
    "iod": "I",
    "phosphor": "P",
    "schwefel": "S"
}