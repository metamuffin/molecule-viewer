/// <reference lib="dom" />

import { Matrix3, Transform } from "./transform.ts";

export interface Drawable {
    draw(c: CanvasRenderingContext2D, transform: Transform, config: DrawConfig): void
    tick(delta: number): void
}

export interface DrawConfig {
    skeleton?: boolean
}

export class Renderer implements Drawable {
    transform: Transform = new Transform()
    keys_down = new Set()
    keys_toggle = new Set()
    private running = true
    canvas: HTMLCanvasElement
    config: DrawConfig = {}
    time = 0

    constructor(public drawable: Drawable) {
        const canvas = this.canvas = document.createElement("canvas")
        canvas.style.position = "absolute"
        canvas.style.top = "0px"
        canvas.style.left = "0px"
        canvas.style.width = "100vw"
        canvas.style.height = "100vh"
        document.body.append(canvas)

        const ctx_ = canvas.getContext("2d")
        if (!ctx_) throw new Error("sdcaxafjhsdf");
        const context = ctx_

        function resize() {
            const w = canvas.getBoundingClientRect().width
            const h = canvas.getBoundingClientRect().height
            if (canvas.width != w) canvas.width = w
            if (canvas.height != h) canvas.height = h
        }
        this.init_events()

        let last_t = Date.now()
        const redraw = () => {
            resize()

            const now = Date.now()
            const delta_t = Math.min((now - last_t) / 1000, 100)
            last_t = now
            this.draw(context)
            this.tick(delta_t)

            if (this.running) requestAnimationFrame(() => redraw())
        }
        redraw()
    }

    init_events() {
        document.body.addEventListener("keydown", ev => this.keys_down.add(ev.code))
        document.body.addEventListener("keyup", ev => this.keys_down.delete(ev.code))
        document.body.addEventListener("keydown", ev => this.keys_toggle.has(ev.code) ? this.keys_toggle.delete(ev.code) : this.keys_toggle.add(ev.code))

        document.body.addEventListener("contextmenu", ev => ev.preventDefault())
        document.body.addEventListener("mousemove", ev => {
            const [left, right] = [ev.buttons & 1, ev.buttons & 2]
            if (left) {
                this.transform.offset.x += ev.movementX
                this.transform.offset.y += ev.movementY
            }
            if (right) {
                this.transform.apply_matrix(Matrix3.rotate_x(ev.movementX * 0.01))
                this.transform.apply_matrix(Matrix3.rotate_y(ev.movementY * 0.01))
            }
        })
        document.body.addEventListener("wheel", ev => {
            this.transform.offset.z += ev.deltaY * 0.2
        })

    }

    tick(delta: number): void {
        this.drawable.tick(delta)
        this.time += delta

        const translate_step = 1000 * delta
        const rotate_step = 2 * delta
        if (this.keys_down.has("KeyA")) this.transform.offset.x += translate_step
        if (this.keys_down.has("KeyD")) this.transform.offset.x -= translate_step
        if (this.keys_down.has("KeyW")) this.transform.offset.y += translate_step
        if (this.keys_down.has("KeyS")) this.transform.offset.y -= translate_step
        if (this.keys_down.has("ShiftLeft")) this.transform.offset.z += translate_step
        if (this.keys_down.has("Space")) this.transform.offset.z -= translate_step
        if (this.keys_down.has("KeyQ")) this.transform.apply_matrix(Matrix3.rotate_z(rotate_step))
        if (this.keys_down.has("KeyE")) this.transform.apply_matrix(Matrix3.rotate_z(-rotate_step))
        if (this.keys_down.has("Digit1")) this.transform.apply_matrix(Matrix3.rotate_y(rotate_step))
        if (this.keys_down.has("Digit3")) this.transform.apply_matrix(Matrix3.rotate_y(-rotate_step))
    }

    draw(context: CanvasRenderingContext2D) {
        context.fillStyle = "black"
        context.fillRect(0, 0, context.canvas.width, context.canvas.height)

        context.save()
        this.drawable.draw(context, this.transform, this.config)
        context.restore()
    }

    destroy() {
        this.running = false
        this.canvas.remove()
    }
}

// export class Transform {
//     x_off = 0
//     y_off = 0
//     scale = 2
//     apply_to_context(c: CanvasRenderingContext2D) {
//         c.translate(c.canvas.width / 2, c.canvas.height / 2)
//         c.scale(this.scale, this.scale)
//         c.translate(this.x_off, this.y_off)
//     }
//     transform(x: number, y: number): { x: number, y: number } {
//         return {
//             x: x * this.scale + this.x_off,
//             y: y * this.scale + this.y_off,
//         }
//     }
//     untransform(x: number, y: number): { x: number, y: number } {
//         return {
//             x: (x - this.x_off) / this.scale,
//             y: (y - this.y_off) / this.scale,
//         }
//     }
// }