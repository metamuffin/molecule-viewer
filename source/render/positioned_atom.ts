/// <reference lib="dom" />
import { DISPLAY_COLOR } from "../data/table.ts";
import { Atom } from "../representation/atom.ts";
import { DrawConfig } from "./renderer.ts";
import { Transform } from "./transform.ts";

export class PositionedAtom {
    x = Math.random() * 200 - 100
    y = Math.random() * 200 - 100
    z = 0

    vx = 0
    vy = 0
    vz = 0

    constructor(public atom: Atom, protected position_mapping: Map<Atom, PositionedAtom>) { }

    draw_bonds(c: CanvasRenderingContext2D, transform: Transform, draw_config: DrawConfig) {
        this.atom.bonds.forEach(bond => {
            const a = this.position_mapping.get(bond.a)!
            const b = this.position_mapping.get(bond.b)!
            if ((a.atom.element == "H" || b.atom.element == "H") && draw_config.skeleton) return

            const dx = a.x - b.x
            const dy = a.y - b.y
            const d = Math.sqrt(dx ** 2 + dy ** 2)
            const nx = dx / d
            const ny = dy / d

            c.strokeStyle = "#ffaaaa60"
            c.lineWidth = 3
            c.lineCap = "round"
            c.beginPath()

            const draw_line_offset = (d: number) => {
                const off_x = -ny * d
                const off_y = nx * d
                transform.mapped(c, a.x, a.y, a.z, () => c.moveTo(off_x, off_y))
                transform.mapped(c, b.x, b.y, b.z, () => c.lineTo(off_x, off_y))
            }

            if (bond.type == "single") draw_line_offset(0)
            if (bond.type == "double") { draw_line_offset(-3); draw_line_offset(3) }
            if (bond.type == "triple") { draw_line_offset(-5); draw_line_offset(0); draw_line_offset(5) }

            c.stroke()
        })
    }

    draw(c: CanvasRenderingContext2D, transform: Transform, draw_config: DrawConfig) {
        if ((this.atom.element == "H" || this.atom.element == "C") && draw_config.skeleton) return

        c.save()
        transform.map(c, this.x, this.y, this.z)

        c.fillStyle = DISPLAY_COLOR[this.atom.element] + "66"
        c.beginPath()
        c.arc(0, 0, 20, 0, Math.PI * 2)
        c.closePath()
        c.fill()
        c.fillStyle = "white"
        c.textAlign = "center"
        c.textBaseline = "middle"
        c.font = "20px sans-serif"
        c.fillText(this.atom.element, 0, 0)

        const charge = this.atom.charge
        if (charge != 0) {
            c.font = "20px sans-serif"
            c.fillText(
                (Math.abs(charge) != 1 ? `${charge}` : "")
                + (charge < 0 ? "-" : "+"),
                15,
                -15)
        }
        c.restore()
    }

}

