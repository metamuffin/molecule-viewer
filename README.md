# molecule-viewer

Web application to visualize molecule structure on a plane

A hosted version is availible on [metamuffin.org](https://s.metamuffin.org/projects/chemistry/)

## Usage

The molecule to show is passed through the url. Serialized with either the IUPAC nomenclature for organic molecules **in german** or SMILES

### Examples

- `?iupac=ethanol` / `?smiles=CCOH`
- `?iupac=2-fluor-1-methyl-cyclopentenol` / `?smiles=CC1=C(F)C(OH)CC1`
- `?iupac=fluormethyloxocyclopentenol` / `?smiles=CC(F)1C(=O)C(O)=CC1`
