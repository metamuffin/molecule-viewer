/// <reference lib="dom" />

import { Logger } from "./logger.ts";
import { build_iupac_chain } from "./naming/iupac/build.ts";
import { parse_iupac_chain } from "./naming/iupac/parse.ts";
import { build_smiles } from "./naming/smiles/build.ts";
import { parse_smiles } from "./naming/smiles/parse.ts";
import { validate_molecule } from "./naming/validate.ts";
import { Layouter2d } from "./render/layouting/2d.ts";
import { Layouter3d } from "./render/layouting/3d.ts";
import { Renderer } from "./render/renderer.ts";
import { Molecule } from "./representation/molecule.ts";
import { ui_input } from "./ui/input.ts";
const L = new Logger("index")

export const RENDERER = new Renderer({ draw: () => { }, tick: () => { } })

ui_input()
load_from_url(new URL(window.location.href))

// returns success
export function load_from_url(url: URL): boolean {
    try {
        let mol
        const iupac = url.searchParams.get("iupac")
        if (iupac) {
            window.document.title = iupac
            const parsed = parse_iupac_chain(iupac);
            L.log(JSON.stringify(parsed, null, 4));
            mol = new Molecule(build_iupac_chain(parsed)[0])
        }
        const smiles = url.searchParams.get("smiles")
        if (smiles) {
            window.document.title = smiles
            const parsed = parse_smiles(smiles)
            L.log(JSON.stringify(parsed, null, 4));
            mol = build_smiles(parsed)
        }
        if (!mol) return false
        validate_molecule(mol)
        const layouter = url.searchParams.get("layouter") ?? "2d"
        if (layouter == "2d") {
            RENDERER.drawable = new Layouter2d(mol)
        } else if (layouter == "3d") {
            RENDERER.drawable = new Layouter3d(mol)
        } else return L.error("layouter missing"), false
        load_dyn_from_url(url)
        return true
    } catch (e) {
        L.error(e)
        return false
    }
}

export function load_dyn_from_url(url: URL) {
    RENDERER.config.skeleton = url.searchParams.get("skeleton") == "1"
}   

window.onpopstate = () => {
    load_from_url(new URL(window.location.href))
}
