/// <reference lib="dom" />
import { SHELL_COUNT } from "../../data/table.ts";
import { PositionedAtom } from ".././positioned_atom.ts";
import { Drawable, DrawConfig } from ".././renderer.ts"
import { Transform } from "../transform.ts";
import { Layouter } from "./common.ts";


export class Layouter2d extends Layouter implements Drawable {
    draw(c: CanvasRenderingContext2D, transform: Transform, dc: DrawConfig): void {
        c.save()

        this.positions.forEach(p => p.draw_bonds(c, transform, dc))
        this.positions.forEach(p => p.draw(c, transform, dc))

        c.restore()
    }

    tick(delta: number): void {
        delta *= 50
        let c = 0
        while ((delta - c) > 0) {
            const s = Math.min(0.1, delta - c)
            this.tick_t(s)
            c += s
        }
    }

    tick_t(delta: number): void {
        this.positions.forEach(p => {
            this.positions.forEach(p2 => {
                const d = atom_dist(p, p2)
                const f = SHELL_COUNT[p.atom.element] * SHELL_COUNT[p2.atom.element] * 30000 * Math.min(1, 1 / d ** 2)

                const nx = (p.x - p2.x) / d
                const ny = (p.y - p2.y) / d

                p.vx += f * nx
                p.vy += f * ny

            })

            p.atom.bonds.forEach(b => {
                const p2 = this.pos_mapping.get(b.other(p.atom))!

                const d = atom_dist(p, p2)
                const f = -d * 1

                const nx = (p.x - p2.x) / d
                const ny = (p.y - p2.y) / d

                p.vx += f * nx
                p.vy += f * ny
            })

        })

        this.positions.forEach(p => {
            p.x += p.vx * delta
            p.y += p.vy * delta
            p.vx = 0
            p.vy = 0
        })
    }
}

function atom_dist(a: PositionedAtom, b: PositionedAtom): number {
    return Math.sqrt((a.x - b.x) ** 2 + (a.y - b.y) ** 2) || 1
}


