import { ACCEPTED_BONDS, CElement } from "../data/table.ts";
import { Bond, BondType } from "./bond.ts";

export class Atom {
    bonds: Set<Bond> = new Set()
    constructor(
        public element: CElement
    ) { }

    destroy() {
        this.bonds.forEach(b => b.destroy())
    }

    get_bond_to(a: Atom): Bond | undefined {
        return [...this.bonds].find(b => b.has(a))
    }

    bind(o: Atom, type?: BondType): Atom {
        new Bond(this, o, type)
        return this
    }
    get bond_count() { return [...this.bonds].reduce((a, v) => a + v.count, 0) }
    get free_bond_count() { return ACCEPTED_BONDS[this.element] - this.bond_count }
    get charge() { return this.free_bond_count }
    get bond_arr() { return [...this.bonds] }
}
