import { BondType } from "../../representation/bond.ts";

export type SMILESChain = SMILESEl[]

export interface SMILESEl {
    element: string
    bond: BondType
    attachments: SMILESChain[]
    cross_bonds: number[]
    implicit_aromatic_ring_bond?: boolean
}
