// deno-lint-ignore-file no-explicit-any

let warn_callback: undefined | ((s: string) => void)
export const set_warn_callback = (e: (s: string) => void) => warn_callback = e
let error_callback: undefined | ((s: string) => void)
export const set_error_callback = (e: (s: string) => void) => error_callback = e

export class Logger {
    constructor(private mod: string) { }
    filter() { return !eval("globalThis.Deno") }
    log(...s: any[]) {
        if (this.filter()) console.log(this.mod, ...s);
    }
    warn(...s: any[]) {
        if (this.filter()) console.warn(this.mod, ...s)
        if (warn_callback) warn_callback(s.join(" "))
    }
    error(...s: any[]) {
        if (this.filter()) console.error(this.mod, ...s)
        if (error_callback) error_callback(s.join(" "))
    }
}
