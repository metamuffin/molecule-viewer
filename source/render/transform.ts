
export interface Vector3 { x: number, y: number, z: number }

export class Transform {
    matrix: Matrix3 = new Matrix3()
    offset: Offset3 = new Offset3()

    constructor() {

    }

    map(c: CanvasRenderingContext2D, x: number, y: number, z: number) {
        c.translate(c.canvas.width / 2, c.canvas.height / 2)

        const { x: mx, y: my, z: mz } = this.offset.map(this.matrix.map({ x, y, z }))
        const mzs = mz * 0.003
        c.scale(1 / (mzs + 1), 1 / (mzs + 1))
        c.translate(mx, my)
    }

    z_index(o: Vector3): number {
        return this.matrix.map(o).z
    }

    mapped(c: CanvasRenderingContext2D, x: number, y: number, z: number, cn: () => void) {
        c.save()
        this.map(c, x, y, z)
        cn()
        c.restore()
    }

    apply_matrix(m: Matrix3) {
        this.matrix = Matrix3.combine(this.matrix, m)
    }
}

export class Offset3 {
    constructor(
        public x = 0, public y = 0, public z = 0
    ) { }
    map({ x, y, z }: Vector3): Vector3 {
        return { x: x + this.x, y: y + this.y, z: z + this.z }
    }
}

export class Matrix3 {
    constructor(
        public xx = 1, public xy = 0, public xz = 0,
        public yx = 0, public yy = 1, public yz = 0,
        public zx = 0, public zy = 0, public zz = 1,
    ) { }

    static combine(x: Matrix3, y: Matrix3) {
        return new Matrix3(
            x.xx * y.xx + x.xy * y.yx + x.xz * y.zx, x.xx * y.xy + x.xy * y.yy + x.xz * y.zy, x.xx * y.xz + x.xy * y.yz + x.xz * y.zz,
            x.yx * y.xx + x.yy * y.yx + x.yz * y.zx, x.yx * y.xy + x.yy * y.yy + x.yz * y.zy, x.yx * y.xz + x.yy * y.yz + x.yz * y.zz,
            x.zx * y.xx + x.zy * y.yx + x.zz * y.zx, x.zx * y.xy + x.zy * y.yy + x.zz * y.zy, x.zx * y.xz + x.zy * y.yz + x.zz * y.zz
        )
    }

    map({ x, y, z }: Vector3): Vector3 {
        return {
            x: this.xx * x + this.xy * y + this.xz * z,
            y: this.yx * x + this.yy * y + this.yz * z,
            z: this.zx * x + this.zy * y + this.zz * z,
        }
    }

    static rotate_z(alpha: number): Matrix3 {
        const c = Math.cos(alpha)
        const s = Math.sin(alpha)
        return new Matrix3(
            c, -s, 0,
            s, c, 0,
            0, 0, 1
        )
    }
    static rotate_y(alpha: number): Matrix3 {
        const c = Math.cos(alpha)
        const s = Math.sin(alpha)
        return new Matrix3(
            c, 0, -s,
            0, 1, 0,
            s, 0, c
        )
    }
    static rotate_x(alpha: number): Matrix3 {
        const c = Math.cos(alpha)
        const s = Math.sin(alpha)
        return new Matrix3(
            1, 0, 0,
            0, c, -s,
            0, s, c
        )
    }
}