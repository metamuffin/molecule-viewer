import { Atom } from "../../representation/atom.ts";
import { Molecule } from "../../representation/molecule.ts";
import { PositionedAtom } from "../positioned_atom.ts";

export abstract class Layouter {
    protected positions: PositionedAtom[] = [] // ordered by transformed z

    protected pos_mapping: Map<Atom, PositionedAtom> = new Map()

    constructor(public molecule: Molecule) {
        molecule.atoms().forEach(atom => {
            const pa = new PositionedAtom(atom, this.pos_mapping)
            this.positions.push(pa)
            this.pos_mapping.set(atom, pa)
        })
    }

}