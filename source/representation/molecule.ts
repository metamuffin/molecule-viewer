import { Atom } from "./atom.ts";

export class Molecule {
    constructor(public root: Atom) {

    }

    atoms(): Set<Atom> {
        const visited: Set<Atom> = new Set()
        const visit = (a: Atom) => {
            if (visited.has(a)) return
            visited.add(a)
            a.bonds.forEach(b => {
                visit(b.other(a))
            })
        }
        visit(this.root)
        return visited
    }
}

