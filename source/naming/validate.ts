import { Atom } from "../representation/atom.ts";
import { Molecule } from "../representation/molecule.ts";

export function validate_molecule(m: Molecule) {

    m.atoms().forEach(a => {
        if (a.free_bond_count < 0) console.warn(`${a.element}-atom has ${-a.free_bond_count} too many bonds`);

        const acc = new Set<Atom>()
        for (const b of a.bonds) {
            if (acc.has(b.other(a))) {
                console.warn(`two independent bonds between ${a.element} and ${b.other(a).element}`);
            }
            acc.add(b.other(a))
        }

    })

}