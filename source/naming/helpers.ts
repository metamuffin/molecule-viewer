import { ACCEPTED_BONDS } from "../data/table.ts";
import { Atom } from "../representation/atom.ts";
import { Bond } from "../representation/bond.ts";
import { Molecule } from "../representation/molecule.ts";

export function link_chain(c: Atom[]) {
    for (let i = 0; i < c.length - 1; i++) {
        new Bond(c[i], c[i + 1])
    }
}

export function saturate_hydrogen(m: Molecule) {
    for (const a of m.atoms()) {
        const max_b = ACCEPTED_BONDS[a.element]
        const b = a.bond_count
        const add_b = max_b - b
        for (let i = 0; i < add_b; i++) {
            new Bond(a, new Atom("H"))
        }
    }
}