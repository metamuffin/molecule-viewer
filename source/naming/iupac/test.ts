import { set_warn_callback } from "../../logger.ts";
import { build_iupac_chain } from "./build.ts";
import { parse_iupac_chain } from "./parse.ts";

let total = 0, succ = 0, ssucc = 0, warns = 0
function testcase(s: string) {
    total += 1
    try {
        set_warn_callback(m => { console.warn(`warning for ${JSON.stringify(s)}: ` + m); warns += 1 })
        const wb = warns
        build_iupac_chain(parse_iupac_chain(s))
        if (warns > wb) ssucc += 1
        else succ += 1
    } catch (e) {
        console.log(`Test case for ${JSON.stringify(s)} failed: ${e}`);
    }
}

[
    "methan",
    "ethanol",
    "2-methylpropan",
    "2-methyl-propan",
    "fluormethyloxocyclopentenol",
    "1-fluor-2-methyl-3-oxo-cyclopent-4-en-2-ol",
    "aminomethan",
    "methylamin",
    "ethansäure",
    "diethyläther",
    "cyclohex-1,3,5-trien",
].forEach(testcase)

console.log(`total:             ${total}`);
console.log(`successful:        ${succ}`);
console.log(`semi-successful:   ${ssucc}`);
console.log(`failed:            ${total - ssucc - succ}`);

