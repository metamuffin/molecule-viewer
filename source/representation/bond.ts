import { Atom } from "./atom.ts";

export type BondType = "single" | "double" | "triple"

export class Bond {
    constructor(
        public a: Atom,
        public b: Atom,
        public type: BondType = "single"
    ) {
        this.a.bonds.add(this)
        this.b.bonds.add(this)
    }
    destroy() {
        this.a.bonds.delete(this)
        this.b.bonds.delete(this)
    }

    other(t: Atom) {
        if (t == this.a) return this.b
        if (t == this.b) return this.a
        throw new Error("err!");
    }
    has(t: Atom) {
        return this.a == t || this.b == t
    }

    get count() { return { single: 1, double: 2, triple: 3 }[this.type] }

}