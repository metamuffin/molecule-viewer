
```js
const d = JSON.parse(await (await fetch("https://raw.githubusercontent.com/Bowserinator/Periodic-Table-JSON/master/periodic-table-lookup.json")).text())
const n = k => k > 4 ? 8 - k : k
Object.fromEntries(d.order.map(o => [d[o].symbol,n(d[o].shells[d[o].shells.length-1])]))
```
