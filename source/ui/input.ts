import { load_dyn_from_url, load_from_url } from "../index.ts";
import { set_error_callback, set_warn_callback } from "../logger.ts";

export function ui_input() {
    document.body.append(build_el())
}

function build_el(): HTMLElement {
    const el = document.createElement("div")
    el.onkeydown = ev => ev.cancelBubble = true
    el.onkeyup = ev => ev.cancelBubble = true

    el.style.zIndex = "10"
    el.style.position = "absolute"
    el.style.left = "0.5em"
    el.style.top = "0.5em"

    const el_error = document.createElement("pre")
    el_error.style.color = "white"
    el_error.textContent = "no errors reported"

    const el_warn = document.createElement("pre")
    el_warn.style.color = "yellow"
    el_warn.textContent = ""

    const el_type = document.createElement("select")
    const o1 = document.createElement("option")
    o1.textContent = "IUPAC"
    o1.value = "iupac"
    el_type.append(o1)
    const o2 = document.createElement("option")
    o2.textContent = "SMILES"
    o2.value = "smiles"
    el_type.append(o2)
    el_type.value = new URL(document.location.href).searchParams.has("smiles") ? "smiles" : "iupac"

    const el_input = document.createElement("input")
    el_input.style.fontSize = "large"
    el_input.style.width = "30em"
    el_input.placeholder = "name"
    el_input.value = new URL(document.location.href).searchParams.get("iupac") ?? new URL(document.location.href).searchParams.get("smiles") ?? ""

    let timer = 0

    el_input.onkeyup = () => {
        el_warn.textContent = ""
        el_input.style.backgroundColor = "#99ff99"
        el_error.style.color = "white"
        el_error.textContent = `no errors reported`
        if (timer) {
            clearTimeout(timer)
            timer = 0
        }
        el_input.style.backgroundColor = "white"
        timer = setTimeout(() => {
            console.clear()
            el_input.style.backgroundColor = "#99ff99"
            el_warn.textContent = ""

            const url = new URL(window.location.href)
            url.searchParams.delete("iupac")
            url.searchParams.delete("smiles")
            url.searchParams.set(el_type.value, el_input.value)
            if (load_from_url(url))
                window.history.pushState({}, "", url)
        }, 500)
    }

    set_warn_callback(s => {
        el_warn.textContent += s + "\n"
    })

    set_error_callback(e => {
        el_input.style.backgroundColor = "#ff9999"
        el_error.style.color = "red"
        el_error.textContent = `error: ${e}`
    })

    el.append(el_type, el_input, el_error, el_warn, build_render_config())
    return el
}

function build_render_config(): HTMLElement {
    const el = document.createElement("div")

    const checkbox = (id: string, name: string, ob: { value: boolean }) => {
        const e = document.createElement("input")
        e.type = "checkbox"
        e.value = "0"
        e.id = "rc-" + id
        e.checked = ob.value
        e.onchange = () => ob.value = e.checked
        const label = document.createElement("label")
        label.setAttribute("for", "rc" + id)
        label.textContent = name
        label.style.color = "white"
        label.style.fontFamily = "sans-serif"
        el.append(e, label, document.createElement("br"))
    }

    checkbox("skeleton", "Show skeleton", {
        get value(): boolean {
            return new URL(window.location.href).searchParams.get("skeleton") == "1"
        },
        set value(v: boolean) {
            const url = new URL(window.location.href)
            if (v) url.searchParams.set("skeleton", "1")
            else url.searchParams.delete("skeleton")
            load_dyn_from_url(url)
            history.replaceState({}, "", url)
        }
    })

    checkbox("layouting-mode", "Use 3D layouting", {
        get value() {
            return new URL(window.location.href).searchParams.get("layouter") == "3d"
        },
        set value(v: boolean) {
            const url = new URL(window.location.href)
            url.searchParams.set("layouter", v ? "3d" : "2d")
            load_from_url(url)
            history.replaceState({}, "", url)
        }
    })

    return el
}